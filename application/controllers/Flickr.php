<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flickr extends CI_Controller {
	
	public function index(){
		$jsonp = file_get_contents('https://api.flickr.com/services/feeds/photos_public.gne?format=json');
		$data['decoded'] = $this->jsonp_decode($jsonp);
		
		$this->load->view('templates/flickr/header');
		$this->load->view('templates/flickr/index', $data);
		$this->load->view('templates/flickr/scripts');
		$this->load->view('templates/flickr/footer');
	}
	
	private function jsonp_decode($jsonp, $assoc = false) {
		if($jsonp[0] !== '[' && $jsonp[0] !== '{') {
		   $jsonp = substr($jsonp, strpos($jsonp, '('));
		}
		return json_decode(trim($jsonp,'();'), $assoc);
	}
}