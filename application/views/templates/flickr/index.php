<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container gallery-container">

    <h1>Bootstrap 4 Flickr Gallery</h1>

    <p class="page-description text-center">Fluid Layout With Baguettebox</p>
    
    <div class="tz-gallery">

        <div class="row">
			
			<?foreach($decoded->items as $item){?>
            <div class="col-sm-12 col-md-4 img-wrap">
                <a class="lightbox" href="<?=$item->media->m;?>">
                    <img src="<?=$item->media->m;?>" alt="<?=$item->title;?>">
                </a>
            </div>
			<?}?>

        </div>

    </div>

</div>